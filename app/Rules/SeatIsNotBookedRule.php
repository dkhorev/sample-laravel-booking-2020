<?php

namespace App\Rules;

use App\Contracts\Repositories\SeatRepoContract;
use Exception;
use Illuminate\Contracts\Validation\Rule;

class SeatIsNotBookedRule implements Rule
{
    /**
     * @var SeatRepoContract
     */
    protected $seatRepo;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(SeatRepoContract $seatRepo)
    {
        $this->seatRepo = $seatRepo;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $seatId
     *
     * @return bool
     */
    public function passes($attribute, $seatId)
    {
        try {
            $this->seatRepo->filterAvailable()->findById($seatId);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.seat.already_booked');
    }
}
