<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneValidRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $phone
     * @return bool
     */
    public function passes($attribute, $phone)
    {
        $phone = str_replace(['(', ')', '-', ' ', '+', '.'], '', $phone);

        return (bool)(
            (preg_match('/^\+?\d+$/', $phone) && strlen($phone) >= 8 && strlen($phone) <= 13)
        );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.visitor.phone.error');
    }
}
