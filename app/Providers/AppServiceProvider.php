<?php

namespace App\Providers;

use App\Contracts\Services\ImageSaveServiceContract;
use App\Contracts\Services\PlaceListServiceContract;
use App\Contracts\Services\SeatBookServiceContract;
use App\Contracts\Services\SeatListServiceContract;
use App\Contracts\Services\VisitorCreateServiceContract;
use App\Services\ImageSaveService;
use App\Services\PlaceListService;
use App\Services\SeatBookService;
use App\Services\SeatListService;
use App\Services\VisitorCreateService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PlaceListServiceContract::class, PlaceListService::class);
        $this->app->bind(SeatListServiceContract::class, SeatListService::class);
        $this->app->bind(VisitorCreateServiceContract::class, VisitorCreateService::class);
        $this->app->bind(ImageSaveServiceContract::class, ImageSaveService::class);
        $this->app->bind(SeatBookServiceContract::class, SeatBookService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
