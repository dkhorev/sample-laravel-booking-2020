<?php

namespace App\Providers;

use App\Contracts\Repositories\EventRepoContract;
use App\Contracts\Repositories\ImageRepoContract;
use App\Contracts\Repositories\PlaceRepoContract;
use App\Contracts\Repositories\SeatRepoContract;
use App\Contracts\Repositories\VisitorRepoContract;
use App\Repositories\EventRepo;
use App\Repositories\ImageRepo;
use App\Repositories\PlaceRepo;
use App\Repositories\SeatRepo;
use App\Repositories\VisitorRepo;
use Illuminate\Support\ServiceProvider;

class AppRepositoryProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EventRepoContract::class, EventRepo::class);
        $this->app->bind(PlaceRepoContract::class, PlaceRepo::class);
        $this->app->bind(VisitorRepoContract::class, VisitorRepo::class);
        $this->app->bind(SeatRepoContract::class, SeatRepo::class);
        $this->app->bind(ImageRepoContract::class, ImageRepo::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
