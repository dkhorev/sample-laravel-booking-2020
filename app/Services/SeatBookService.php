<?php

namespace App\Services;

use App\Contracts\Repositories\SeatRepoContract;
use App\Contracts\Services\ImageSaveServiceContract;
use App\Contracts\Services\SeatBookServiceContract;
use App\Events\Services\SeatBookedEvent;
use App\Exceptions\SeatBookServiceException;
use App\Exceptions\SeatNotAvailableException;
use App\Models\Seat;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Throwable;

class SeatBookService implements SeatBookServiceContract
{
    /**
     * Event to fire when bokking is succesful
     *
     * @var string
     */
    protected $successEvent = SeatBookedEvent::class;

    /**
     * @var SeatRepoContract
     */
    protected $seatRepo;

    /**
     * @var VisitorCreateService
     */
    protected $visitorCreateService;

    /**
     * @var ImageSaveServiceContract
     */
    protected $imageSaveService;

    /**
     * @var int
     */
    private $eventId;

    /**
     * @var UploadedFile
     */
    private $image;

    /**
     * @var int
     */
    private $seatId;

    /**
     * @var Seat
     */
    private $seat;

    /**
     * SeatBookService constructor.
     *
     * @param SeatRepoContract         $seatRepo
     * @param VisitorCreateService     $visitorCreate
     * @param ImageSaveServiceContract $imageSaveService
     */
    public function __construct(
        SeatRepoContract $seatRepo,
        VisitorCreateService $visitorCreate,
        ImageSaveServiceContract $imageSaveService
    ) {
        $this->seatRepo = $seatRepo;
        $this->visitorCreateService = $visitorCreate;
        $this->imageSaveService = $imageSaveService;
    }

    /**
     * @param int $id
     *
     * @return $this
     * @throws SeatNotAvailableException
     */
    public function setSeat(int $id): SeatBookServiceContract
    {
        try {
            /** @var Seat $seat */
            $seat = $this->seatRepo->reset()
                                   ->filterAvailable()
                                   ->findById($id);

            $this->seatId = $id;
            $this->eventId = $seat->event->id;

            return $this;
        } catch (Exception $e) {
            throw new SeatNotAvailableException();
        }
    }

    /**
     * @param UploadedFile $image
     *
     * @return $this
     */
    public function setImage(UploadedFile $image): SeatBookServiceContract
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @param array $visitorData
     *
     * @return Seat
     * @throws SeatBookServiceException
     * @throws Throwable
     */
    public function bookFor(array $visitorData): Seat
    {
        $this->validateInput();

        DB::transaction(function () use ($visitorData) {
            /** @var Seat $seat */
            $seat = $this->seatRepo->reset()
                                   ->findById($this->seatId, $lock = true);

            $visitor = $this->visitorCreateService->createForSeat($visitorData, $seat->id);

            $this->imageSaveService->saveForVisitor($this->image, $visitor->id);

            $seat->setBooked();

            $this->seat = $seat;
        });

        event($this->successEvent);

        return $this->seat;
    }

    /**
     * @return $this
     * @throws SeatBookServiceException
     */
    private function validateInput()
    {
        if (!$this->image || !$this->seatId || !$this->eventId) {
            throw new SeatBookServiceException('Wrong config.');
        }

        return $this;
    }
}
