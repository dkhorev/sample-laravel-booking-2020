<?php

namespace App\Services;

use App\Contracts\Repositories\VisitorRepoContract;
use App\Contracts\Services\VisitorCreateServiceContract;
use App\Models\Visitor;
use Illuminate\Database\QueryException;

class VisitorCreateService implements VisitorCreateServiceContract
{
    /**
     * @var VisitorRepoContract
     */
    protected $visitorRepo;

    /**
     * VisitorCreateService constructor.
     *
     * @param VisitorRepoContract $visitorRepo
     */
    public function __construct(VisitorRepoContract $visitorRepo)
    {
        $this->visitorRepo = $visitorRepo;
    }

    /**
     * Add new visitor base on passed data
     *
     * @param array $data
     * @param int   $seatId
     *
     * @return Visitor
     * @throws QueryException
     */
    public function createForSeat(array $data, int $seatId): Visitor
    {
        /** @var Visitor $visitor */
        $visitor = $this->visitorRepo->create(array_merge($data, [
            'seat_id' => $seatId,
        ]));

        return $visitor;
    }
}
