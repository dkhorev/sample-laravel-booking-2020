<?php

namespace App\Services;

use App\Contracts\Repositories\PlaceRepoContract;
use App\Contracts\Services\PlaceListServiceContract;
use Illuminate\Support\Collection;

class PlaceListService implements PlaceListServiceContract
{
    /**
     * @var PlaceRepoContract
     */
    protected $placeRepo;

    /**
     * PlaceListService constructor.
     *
     * @param PlaceRepoContract $placeRepo
     */
    public function __construct(PlaceRepoContract $placeRepo)
    {
        $this->placeRepo = $placeRepo;
    }

    /**
     * Retrieve all Places collection
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->placeRepo->get();
    }
}
