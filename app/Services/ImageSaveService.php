<?php

namespace App\Services;

use App\Contracts\Repositories\ImageRepoContract;
use App\Contracts\Services\ImageSaveServiceContract;
use App\Models\Image;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;

class ImageSaveService implements ImageSaveServiceContract
{
    /**
     * @var ImageRepoContract
     */
    protected $imageRepo;

    /**
     * ImageSaveService constructor.
     *
     * @param ImageRepoContract $imageRepo
     */
    public function __construct(ImageRepoContract $imageRepo)
    {
        $this->imageRepo = $imageRepo;
    }

    /**
     * Save image from uploaded data and link to Visitor model
     * todo validate Visitor exists
     *
     * @param UploadedFile $file
     * @param int          $visitorId
     *
     * @return Image
     * @throws QueryException
     */
    public function saveForVisitor(UploadedFile $file, int $visitorId): Image
    {
        /** @var Image $image */
        $image = $this->imageRepo->create([
            'filename'   => $file->getClientOriginalName(),
            'mime'       => $file->getMimeType(),
            'ext'        => $file->getClientOriginalExtension(),
            'size'       => $file->getSize(),
            'visitor_id' => $visitorId,
        ]);

        $file->storeAs(
            Image::PATH,
            $image->getFilenameById(),
            'public'
        );

        return $image;
    }
}
