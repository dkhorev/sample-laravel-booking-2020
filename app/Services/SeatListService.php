<?php

namespace App\Services;

use App\Contracts\Repositories\EventRepoContract;
use App\Contracts\Repositories\SeatRepoContract;
use App\Contracts\Services\SeatListServiceContract;
use App\Exceptions\EventNotFoundException;
use App\Models\Seat;
use Exception;
use Illuminate\Support\Collection;

class SeatListService implements SeatListServiceContract
{
    /**
     * @var SeatRepoContract
     */
    protected $seatRepo;

    /**
     * @var EventRepoContract
     */
    protected $eventRepo;

    /**
     * SeatListService constructor.
     *
     * @param SeatRepoContract  $seatRepo
     * @param EventRepoContract $eventRepo
     */
    public function __construct(SeatRepoContract $seatRepo, EventRepoContract $eventRepo)
    {
        $this->seatRepo = $seatRepo;
        $this->eventRepo = $eventRepo;
    }

    /**
     * Retrieve all seats by event Id
     *
     * @param int $id
     *
     * @return Collection|Seat
     * @throws EventNotFoundException
     */
    public function allForEventId(int $id): Collection
    {
        $this->validateEvent($id);

        return $this->seatRepo->filterByEventId($id)->get();
    }

    /**
     * @param int $id
     *
     * @throws EventNotFoundException
     */
    private function validateEvent(int $id)
    {
        try {
            $this->eventRepo->findById($id);
        } catch (Exception $e) {
            throw new EventNotFoundException("Event does not exists: {$id}");
        }
    }
}
