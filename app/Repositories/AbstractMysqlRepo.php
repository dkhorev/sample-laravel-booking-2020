<?php

namespace App\Repositories;

use App\Contracts\Repositories\RepoContract;
use App\Models\BaseModel;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class AbstractMysqlRepo implements RepoContract
{
    /**
     * @var Model|Builder|BaseModel
     */
    protected $model;

    /**
     * Repo constructor
     *
     * Init empty builder
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Set default builder to model property
     *
     * @return mixed
     */
    abstract protected function init(): RepoContract;

    /**
     * Reset repo filters
     *
     * @return RepoContract
     */
    public function reset(): RepoContract
    {
        return $this->init();
    }

    /**
     * Take the results
     *
     * @return Collection|static[]
     */
    public function get(): Collection
    {
        return $this->model->get();
    }

    /**
     * Retrieve instant model by Id
     *
     * You could use locking if need. Variable type is similar like in Builder::lock()
     *
     * @param      $id
     * @param null $lock (false = sharedLock(), true = lockForUpdate())
     *
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|Model
     */
    public function findById($id, $lock = null)
    {
        if (isset($lock)) {
            $this->model->lock($lock);
        }

        return $this->model->findOrFail($id);
    }

    /**
     * Create new model
     *
     * @param array $data
     *
     * @return BaseModel
     */
    public function create(array $data): BaseModel
    {
        return $this->model->create($data);
    }

    /**
     * Delete model
     *
     * @param Model $model
     *
     * @return bool
     * @throws Exception
     */
    public function delete(Model $model): bool
    {
        return $this->model->delete($model);
    }
}
