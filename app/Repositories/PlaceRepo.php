<?php

namespace App\Repositories;

use App\Contracts\Repositories\PlaceRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Place;

class PlaceRepo extends AbstractMysqlRepo implements PlaceRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = Place::with([
            'events',
            'events.seats',
            'events.visitors',
        ]);

        return $this;
    }
}
