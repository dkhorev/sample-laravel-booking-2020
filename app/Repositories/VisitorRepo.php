<?php

namespace App\Repositories;

use App\Contracts\Repositories\VisitorRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Visitor;

class VisitorRepo extends AbstractMysqlRepo implements VisitorRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = Visitor::with([
            'seat',
            'image',
        ]);

        return $this;
    }
}
