<?php

namespace App\Repositories;

use App\Contracts\Repositories\ImageRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Image;

class ImageRepo extends AbstractMysqlRepo implements ImageRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = Image::with([
            'visitor',
        ]);

        return $this;
    }
}
