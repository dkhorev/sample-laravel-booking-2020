<?php

namespace App\Repositories;

use App\Contracts\Repositories\EventRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Event;

class EventRepo extends AbstractMysqlRepo implements EventRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = Event::with([
            'place',
            'seats',
            'visitors',
        ]);

        return $this;
    }
}
