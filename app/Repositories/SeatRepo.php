<?php

namespace App\Repositories;

use App\Contracts\Repositories\RepoContract;
use App\Contracts\Repositories\SeatRepoContract;
use App\Models\Seat;

/**
 * Class SeatRepo
 *
 * @package App\Repositories
 */
class SeatRepo extends AbstractMysqlRepo implements SeatRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = Seat::with([
            'event',
            'visitor',
        ]);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function filterByEventId(int $id): SeatRepoContract
    {
        $this->model->where('event_id', $id);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function filterAvailable(): SeatRepoContract
    {
        $this->model->where('booked_at', null);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function filterRowNumber(int $number): SeatRepoContract
    {
        $this->model->where('row_number', $number);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function filterSeatNumber(int $number): SeatRepoContract
    {
        $this->model->where('seat_number', $number);

        return $this;
    }
}
