<?php

namespace App\Contracts\Models;

interface SeatContract
{
    /**
     * Get price in currency
     *
     * @return float
     */
    public function accuracyPrice(): float;

    /**
     * Get logo if place is booked
     *
     * @return mixed
     */
    public function visitorLogo();
}
