<?php

namespace App\Contracts\Repositories;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Interface RepoContract
 *
 * @package App\Contracts\Repositories
 */
interface RepoContract
{
    /**
     * Get results
     *
     * @return Collection|RepoContract[]
     */
    public function get(): Collection;

    /**
     * Reset all filters
     *
     * @return RepoContract
     */
    public function reset(): RepoContract;

    /**
     * Retrieve instant model by Id
     *
     * You could use locking if need. Variable type is similar like in Builder::lock()
     *
     * @param      $id
     * @param null $lock (false = sharedLock(), true = lockForUpdate())
     *
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|Model
     */
    public function findById($id, $lock = null);

    /**
     * Create new model
     *
     * @param array $data
     *
     * @return BaseModel
     */
    public function create(array $data): BaseModel;

    /**
     * Delete model
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool;
}
