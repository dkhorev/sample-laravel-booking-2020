<?php

namespace App\Contracts\Repositories;

/**
 * Interface SeatRepoContract
 *
 * @package App\Contracts\Repositories
 */
interface SeatRepoContract extends RepoContract
{
    /**
     * @param int $id
     *
     * @return $this
     */
    public function filterByEventId(int $id): self;

    /**
     * @return $this
     */
    public function filterAvailable(): self;

    /**
     * @param int $number
     *
     * @return $this
     */
    public function filterRowNumber(int $number): self;

    /**
     * @param int $number
     *
     * @return $this
     */
    public function filterSeatNumber(int $number): self;
}
