<?php

namespace App\Contracts\Services;

use Illuminate\Support\Collection;

interface PlaceListServiceContract
{
    /**
     * Retrieve all Places collection
     *
     * @return Collection
     */
    public function all(): Collection;
}
