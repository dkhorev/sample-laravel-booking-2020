<?php

namespace App\Contracts\Services;

use App\Exceptions\SeatBookServiceException;
use App\Exceptions\SeatNotAvailableException;
use App\Models\Seat;
use Illuminate\Http\UploadedFile;
use Throwable;

interface SeatBookServiceContract
{
    /**
     * @param int $id
     *
     * @return $this
     * @throws SeatNotAvailableException
     */
    public function setSeat(int $id): self;

    /**
     * @param UploadedFile $image
     *
     * @return $this
     */
    public function setImage(UploadedFile $image): self;

    /**
     * @param array $visitorData
     *
     * @return Seat
     * @throws SeatBookServiceException
     * @throws Throwable
     */
    public function bookFor(array $visitorData): Seat;
}
