<?php

namespace App\Contracts\Services;

use App\Exceptions\EventNotFoundException;
use App\Models\Seat;
use Illuminate\Support\Collection;

/**
 * Interface SeatListServiceContract
 *
 * @package App\Contracts\Services
 */
interface SeatListServiceContract
{
    /**
     * Retrieve all seats by event Id
     *
     * @param int $id
     *
     * @return Collection|Seat
     * @throws EventNotFoundException
     */
    public function allForEventId(int $id): Collection;
}
