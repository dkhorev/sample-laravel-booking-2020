<?php

namespace App\Contracts\Services;

use App\Models\Image;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;

/**
 * Interface ImageSaveServiceContract
 *
 * @package App\Contracts\Services
 */
interface ImageSaveServiceContract
{
    /**
     * Save image from uploaded data and link to Visitor model
     *
     * @param UploadedFile $file
     * @param int          $visitorId
     *
     * @return Image
     * @throws QueryException
     */
    public function saveForVisitor(UploadedFile $file, int $visitorId): Image;
}
