<?php

namespace App\Contracts\Services;

use App\Models\Visitor;
use Illuminate\Database\QueryException;

interface VisitorCreateServiceContract
{
    /**
     * Add new visitor base on passed data
     *
     * @param array $data
     * @param int   $seatId
     *
     * @return Visitor
     * @throws QueryException
     */
    public function createForSeat(array $data, int $seatId): Visitor;
}
