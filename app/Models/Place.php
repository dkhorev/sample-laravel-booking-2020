<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class Place
 *
 * @property int              $id
 * @property string           $name
 * @property string           $address
 * @property string           $phone
 * @property float            $lon
 * @property float            $lat
 * @property Carbon           $created_at
 * @property Event|Collection $events
 * @package App\Models
 */
class Place extends BaseModel
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'address',
        'phone',
        'lon',
        'lat',
        'created_at',
    ];

    protected $dates = [
        'created_at',
    ];

    /**
     * @return HasMany
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
