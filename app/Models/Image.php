<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

/**
 * Class Image
 *
 * @property int     $id
 * @property string  $filename
 * @property string  $mime
 * @property string  $ext
 * @property string  $size
 * @property int     $visitor_id
 * @property Visitor $visitor
 * @package App\Models
 */
class Image extends BaseModel
{
    const PATH = 'images';

    public $timestamps = false;

    protected $fillable = [
        'filename',
        'mime',
        'ext',
        'size',
        'visitor_id',
    ];

    /**
     * @return BelongsTo
     */
    public function visitor()
    {
        return $this->belongsTo(Visitor::class);
    }

    /**
     * todo must be in ImageContract
     *
     * @return string
     */
    public function getFilenameById(): string
    {
        return $this->id . '.' . $this->ext;
    }

    /**
     * todo must be in ImageContract
     *
     * @return string
     */
    public function getStoragePath(): string
    {
        return static::PATH . '/' . $this->getFilenameById();
    }

    /**
     * todo must be in ImageContract
     *
     * @return string
     */
    public function getUrl(): string
    {
        return url(Storage::url(static::PATH . '/' . $this->getFilenameById()));
    }
}
