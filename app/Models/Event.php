<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * Class Event
 *
 * @property int                $id
 * @property string             $name
 * @property int                $place_id
 * @property Carbon             $starts_at
 * @property Carbon             $finished_at
 * @property Place              $place
 * @property Seat|Collection    $seats
 * @property Visitor|Collection $visitors
 * @package App\Models
 */
class Event extends BaseModel
{
    use SoftDeletes;

    const DELETED_AT = 'finished_at';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'place_id',
        'starts_at',
        'finished_at',
    ];

    protected $dates = [
        'starts_at',
        'finished_at',
    ];

    /**
     * @return BelongsTo
     */
    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    /**
     * @return hasMany
     */
    public function seats()
    {
        return $this->hasMany(Seat::class);
    }

    /**
     * @return HasManyThrough
     */
    public function visitors()
    {
        return $this->hasManyThrough(Visitor::class, Seat::class);
    }
}
