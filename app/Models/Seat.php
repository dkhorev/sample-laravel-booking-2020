<?php

namespace App\Models;

use App\Contracts\Models\SeatContract;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Seat
 *
 * @property int     $id
 * @property int     $event_id
 * @property int     $row_number
 * @property int     $seat_number
 * @property int     $price
 * @property Carbon  $booked_at
 * @property Event   $event
 * @property Visitor $visitor
 * @package App\Models
 */
class Seat extends BaseModel implements SeatContract
{
    /**
     * Accuracy for converting coins <=> currency
     * todo in real app should be part of currency model
     */
    const PRICE_ACCURACY = 100;

    public $timestamps = false;

    protected $fillable = [
        'event_id',
        'row_number',
        'seat_number',
        'price',
        'booked_at',
    ];

    protected $dates = [
        'booked_at',
    ];

    /**
     * @return BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return HasOne
     */
    public function visitor()
    {
        return $this->hasOne(Visitor::class);
    }

    /**
     * todo in real app - should be part of SeatContract
     */
    public function setBooked()
    {
        $this->booked_at = now();
        $this->save();
    }

    /**
     * Get price in currency
     *
     * @return float
     */
    public function accuracyPrice(): float
    {
        return $this->price / static::PRICE_ACCURACY;
    }

    /**
     * Get logo if place is booked
     *
     * @return mixed
     */
    public function visitorLogo()
    {
        if (is_null($this->booked_at)) {
            return null;
        }

        if (is_null($this->visitor->image)) {
            return null;
        }

        return $this->visitor->image->getUrl();
    }
}
