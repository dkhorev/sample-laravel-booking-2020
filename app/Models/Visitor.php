<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Visitor
 *
 * @property int    $id
 * @property string $name
 * @property string $company
 * @property string $email
 * @property string $phone
 * @property string $desc
 * @property int    $seat_id
 * @property Seat   $seat
 * @property Image  $image
 * @package App\Models
 */
class Visitor extends BaseModel
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'company',
        'email',
        'phone',
        'desc',
        'seat_id',
    ];

    /**
     * @return BelongsTo
     */
    public function seat()
    {
        return $this->belongsTo(Seat::class);
    }

    /**
     * @return HasOne
     */
    public function image()
    {
        return $this->hasOne(Image::class);
    }
}
