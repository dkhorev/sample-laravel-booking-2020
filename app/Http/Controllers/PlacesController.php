<?php

namespace App\Http\Controllers;

use App\Contracts\Services\PlaceListServiceContract;
use App\Http\Resources\PlaceListResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class PlacesController
 *
 * @package App\Http\Controllers
 */
class PlacesController extends Controller
{
    /**
     * @param PlaceListServiceContract $placeListService
     *
     * @return AnonymousResourceCollection
     */
    public function index(PlaceListServiceContract $placeListService)
    {
        return PlaceListResource::collection($placeListService->all());
    }
}
