<?php

namespace App\Http\Controllers;

use App\Contracts\Services\SeatBookServiceContract;
use App\Http\Requests\SeatBookRequest;
use App\Http\Resources\SeatListResource;
use App\Models\Seat;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

/**
 * Class SeatsController
 *
 * @package App\Http\Controllers
 */
class SeatsController extends Controller
{
    /**
     * @param Seat                    $seat
     * @param SeatBookRequest         $request
     * @param SeatBookServiceContract $seatBookService
     *
     * @return Application|JsonResponse|mixed
     */
    public function bookSeat(Seat $seat, SeatBookRequest $request, SeatBookServiceContract $seatBookService)
    {
        try {
            $seatBookService->setSeat($seat->id)
                            ->setImage($request->image)
                            ->bookFor($request->validated());

            return app(SeatListResource::class, ['resource' => $seat->fresh()]);
        } catch (Exception $e) {
        } catch (Throwable $e) {
        }

        return response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
