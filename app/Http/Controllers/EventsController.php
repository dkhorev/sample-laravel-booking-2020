<?php

namespace App\Http\Controllers;

use App\Contracts\Services\SeatListServiceContract;
use App\Exceptions\EventNotFoundException;
use App\Http\Resources\SeatListResource;
use App\Models\Event;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class EventsController
 *
 * @package App\Http\Controllers
 */
class EventsController extends Controller
{
    /**
     * @param Event                   $event
     * @param SeatListServiceContract $seatListService
     *
     * @return AnonymousResourceCollection
     * @throws EventNotFoundException
     */
    public function indexSeats(Event $event, SeatListServiceContract $seatListService)
    {
        return SeatListResource::collection($seatListService->allForEventId($event->id));
    }
}
