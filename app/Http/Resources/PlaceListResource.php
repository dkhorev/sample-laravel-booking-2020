<?php

namespace App\Http\Resources;

use App\Models\Place;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PlaceListResource
 *
 * @property-read Place $resource
 * @package App\Http\Resources
 */
class PlaceListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->resource->id,
            'name'     => $this->resource->name,
            'address'  => $this->resource->address,
            'phone'    => $this->resource->phone,
            'location' => [
                'lon' => $this->resource->lon,
                'lat' => $this->resource->lat,
            ],
            'events'   => EventResource::collection($this->resource->events),
        ];
    }
}
