<?php

namespace App\Http\Resources;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class EventResource
 *
 * @property-read Event $resource
 * @package App\Http\Resources
 */
class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->resource->id,
            'name'          => $this->resource->name,
            'starts_at'     => $this->resource->starts_at->format('d/m/Y'),
            'seat_count'    => $this->resource->seats->count(),
            'visitor_count' => $this->resource->visitors->count(),
        ];
    }
}
