<?php

namespace App\Http\Resources;

use App\Models\Seat;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SeatListResource
 *
 * @property-read Seat $resource
 * @package App\Http\Resources
 */
class SeatListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->resource->id,
            'row_number'   => $this->resource->row_number,
            'seat_number'  => $this->resource->seat_number,
            'price'        => $this->resource->accuracyPrice(),
            'is_booked'    => is_null($this->resource->booked_at) ? false : true,
            'visitor_logo' => $this->resource->visitorLogo(),
            'event_id'     => $this->resource->event_id,
            'place_id'     => $this->resource->event->place_id,
        ];
    }
}
