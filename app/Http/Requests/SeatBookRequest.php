<?php

namespace App\Http\Requests;

use App\Rules\PhoneValidRule;
use App\Rules\SeatIsNotBookedRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\UploadedFile;

/**
 * Class SeatBookRequest
 *
 * @property-read string       $name
 * @property-read string       $company
 * @property-read string       $email
 * @property-read string       $phone
 * @property-read string|null  $desc
 * @property-read UploadedFile $image
 * @package App\Http\Requests
 */
class SeatBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => [
                'required',
                'string',
                'max:255',
            ],
            'company' => [
                'required',
                'string',
                'max:255',
            ],
            'email'   => [
                'required',
                'email',
                'max:255',
            ],
            'phone'   => [
                'required',
                'string',
                app(PhoneValidRule::class),
            ],
            'desc'    => [
                'nullable',
                'string',
                'max:255',
            ],
            'image'   => [
                'required',
                'mimes:jpeg,jpg,png',
                'max:2000', // 2MB max | todo should be in app cofig for real project
            ],
            'seat_id' => [
                app(SeatIsNotBookedRule::class),
            ],
        ];
    }

    /**
     * Get all of the input and files for the request.
     *
     * @param array|mixed|null $keys
     *
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all($keys);

        $data['seat_id'] = $this->route('seat')->id;

        return $data;
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }

}
