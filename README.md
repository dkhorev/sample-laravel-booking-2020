# Backend | Booking app (test job) 01/2020

[Task description](resources/docs/requirements.md)

[Execution plan](plan.md)

#### Setup

`composer i`

`cp .env.exmaple .env`

add your MySQL info (db - create it first, add user and pwd) \
APP_KEY already generated for simplicity

`php artisan storage:link`

`php artisan db:seed`

`php artisan serve`

add IP of running backend to front end project's .env

#### Tests

fill .env.testing with MySQL info (in memory won't work because of sqlite has limitations on foreign keys, integer castings etc)

`composer test`

![Tests](resources/docs/tests.png "Tests")

### Упрощения в реализации

+ В рамках демо приложения и для простоты не продумываем ограничения в БД на длину текстовых полей.
+ Не вводим модель валюты. Все цены в некой абстрактной валюте, однако учтено хранение цен/сумм в копейках и конвертация для ресурса API.
+ Не вводим жесткие контракты на модели (сервисы возвращают модели, но лучше чтобы возвращали интерфейс этой модели).
+ Валидация телефона довольно примитивная (>=8 цифр, <= 13 цифр).
+ Не делаем "очистку" телефона при сохранении.
+ Форма одним компонентом со всей валидацией, на реальном проекте я бы разделил каждый инпут в свой класс. Валидацию тоже отдельно бы вынес в mixin.
+ На фронте не добавлял прелоадеры.
+ На фронте нет валидации телефона (в реальном проекте она была бы очень сложная если разные страны брать в расчет), зато можно увидеть как срабатывает валидация с бэкенда.
