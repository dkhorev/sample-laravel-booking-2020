<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Place;
use Faker\Generator as Faker;

$factory->define(Place::class, function (Faker $faker) {
    return [
        'name'       => ucfirst($faker->unique()->words(2, true)),
        'address'    => $faker->address,
        'phone'      => $faker->e164PhoneNumber,
        'lon'        => $faker->longitude,
        'lat'        => $faker->latitude,
        'created_at' => now(),
    ];
});
