<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Image;
use App\Models\Visitor;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker, $options = []) {
    if (array_key_exists('visitor_id', $options)) {
        $visitor = $options['visitor_id'];
    } else {
        $visitor = factory(Visitor::class)->create();
    }

    return [
        'visitor_id' => $visitor,
        'filename'   => $options['filename'] ?? null,
        'mime'       => $options['mime'] ?? null,
        'ext'        => $options['ext'] ?? null,
        'size'       => $options['size'] ?? null,
    ];
});
