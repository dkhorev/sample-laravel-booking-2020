<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use App\Models\Place;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker, $options = []) {
    if (array_key_exists('place_id', $options)) {
        $place = $options['place_id'];
    } else {
        $place = factory(Place::class)->create();
    }

    return [
        'name'      => $faker->unique()->words(3, true),
        'place_id'  => $place,
        'starts_at' => now(),
    ];
});
