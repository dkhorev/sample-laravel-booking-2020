<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use App\Models\Seat;
use Faker\Generator as Faker;

$factory->define(Seat::class, function (Faker $faker, $options = []) {
    if (array_key_exists('event_id', $options)) {
        $event = $options['event_id'];
    } else {
        $event = factory(Event::class)->create();
    }

    if (array_key_exists('row_number', $options)) {
        $row_number = $options['row_number'];
    } else {
        $row_number = $faker->unique()->numberBetween(1, 50);
    }

    if (array_key_exists('seat_number', $options)) {
        $seat_number = $options['seat_number'];
    } else {
        $seat_number = $faker->unique()->numberBetween(100, 500);
    }

    return [
        'event_id'    => $event,
        'row_number'  => $row_number,
        'seat_number' => $seat_number,
        'price'       => $faker->numberBetween(10000, 50000),
    ];
});
