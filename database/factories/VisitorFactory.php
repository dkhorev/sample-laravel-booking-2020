<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Seat;
use App\Models\Visitor;
use Faker\Generator as Faker;
use \Illuminate\Support\Arr;

$factory->define(Visitor::class, function (Faker $faker, $options = []) {
    if (array_key_exists('seat_id', $options)) {
        $seat = $options['seat_id'];
    } else {
        $seat = factory(Seat::class)->create();
    }

    return [
        'name'    => $faker->name,
        'company' => $faker->company,
        'email'   => $faker->email,
        'phone'   => $faker->e164PhoneNumber,
        'seat_id' => $seat,
        'desc'    => Arr::random([null, $faker->text(100)]),
    ];
});
