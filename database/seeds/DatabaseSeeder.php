<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TruncateTablesSeeder::class);
        $this->call(ImageCleanUpSeeder::class);
        $this->call(PlacesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(SeatsTableSeeder::class);
        $this->call(VisitorsTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
    }
}
