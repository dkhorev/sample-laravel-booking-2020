<?php

use App\Models\Event;
use App\Models\Place;
use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $places = Place::all();

        $places->each(function (Place $place) {
            $times = random_int(1, 3);

            for ($i = 1; $i <= $times; $i++) {
                factory(Event::class)->create([
                    'place_id'  => $place->id,
                    'starts_at' => now()->addDays(random_int(3, 10)),
                ]);
            }
        });
    }
}
