<?php

use App\Models\Place;
use Illuminate\Database\Seeder;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Place::class)->create([
            'lon' => 14.513889,
            'lat' => 35.899096,
        ]);

        factory(Place::class)->create([
            'lon' => 14.508793,
            'lat' => 35.897049,
        ]);

        factory(Place::class)->create([
            'lon' => 14.517792,
            'lat' => 35.892654,
        ]);
    }
}
