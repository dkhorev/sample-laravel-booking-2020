<?php

use App\Models\Image;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class ImageCleanUpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Throwable
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::transaction(function () {
            Image::query()
                 ->chunk(30, function (Collection $images) {
                     $images->each(function (Image $image) {
                         Storage::disk('public')->delete($image->getStoragePath());
                         $image->delete();
                     });
                 });

            DB::table('images')->truncate();
        });
    }
}
