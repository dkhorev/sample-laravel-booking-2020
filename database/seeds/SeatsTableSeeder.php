<?php

use App\Models\Event;
use App\Models\Seat;
use Illuminate\Database\Seeder;

class SeatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = Event::all();

        $events->each(function (Event $event) {
            $rows = random_int(2, 4);
            $seats = random_int(5, 10);

            for ($i = 1; $i <= $rows; $i++) {
                for ($j = 1; $j <= $seats; $j++) {
                    factory(Seat::class)->create([
                        'event_id'    => $event->id,
                        'row_number'  => $i,
                        'seat_number' => $j,
                        'price'       => random_int(10000, 20000),
                    ]);
                }
            }
        });
    }
}
