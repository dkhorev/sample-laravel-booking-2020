<?php

use App\Models\Seat;
use App\Models\Visitor;
use Illuminate\Database\Seeder;

class VisitorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seats = Seat::all();

        $seats->each(function (Seat $seat) {
            $isBooked = (bool)random_int(0, 1);

            if ($isBooked) {
                factory(Visitor::class)->create([
                    'seat_id' => $seat->id,
                ]);
                $seat->setBooked();
            }
        });
    }
}
