<?php

use Illuminate\Database\Seeder;

/**
 * Truncate users and all related tables
 *
 * Class TruncateTablesSeeder
 */
class TruncateTablesSeeder extends Seeder
{
    /**
     * List of tables that are saved
     *
     * @var array
     */
    protected $protectedTables = [
        'images',
        'migrations',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();

        foreach ($tables as $table) {
            if (!in_array($table, $this->protectedTables)) {
                DB::table($table)->truncate();
            }
        }
    }
}
