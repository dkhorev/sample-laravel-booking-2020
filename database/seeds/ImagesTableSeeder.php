<?php

use App\Models\Visitor;
use App\Services\ImageSaveService;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $visitors = Visitor::all();

        $service = app(ImageSaveService::class);

        $visitors->each(function (Visitor $visitor) use ($service) {

            $disk = Storage::disk('local');
            $files = $disk->files('seed_images');
            // dump($files);
            // $file = $disk->path('seed_images/1.png');
            // dd($file);

            $randPath = $disk->path(\Illuminate\Support\Arr::random($files));

            $file = new UploadedFile($randPath, 'test.png');

            $service->saveForVisitor($file, $visitor->id);
        });
    }
}
