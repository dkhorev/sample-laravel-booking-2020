<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('event_id');
            $table->unsignedSmallInteger('row_number');
            $table->unsignedSmallInteger('seat_number');
            $table->unsignedInteger('price')->nullable()->comment('free if null');
            $table->timestamp('booked_at')->nullable()->default(null)->comment('null while not booked');

            $table->unique(['event_id', 'row_number', 'seat_number']);
            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_seats');
    }
}
