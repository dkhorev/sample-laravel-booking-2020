
### Config

default lon,lat for map ?

### Models

Place
    
    +fields - name, address, phone, created_at, lon, lat (point)
    +hasMany - Event
    
Event
    
    +fields - name, place_id, starts_at, finished_at
    +soft deletes
    +belongsTo - Place
    +hasMany - Seat
    +hasManyThrough - Visitor
    
Seat (matrix row number x set number)
    
    +fields - event_id, row_number, seat_number, price, booked_at(null)
    +belongsTo - Event
    +hasOne - Visitor
    
Visitor
    
    +fileds - name, company, email, phone, desc (nullable), seat_id, image_id
    +belongsTo - Seat
    +hasOne - Image

Image
    
    +fields - (filename350, mime30, ext5, size300)nullable, visitor_id
    +belongsTo - Visitor

### Repos for models

abstract MysqlRepository
    
    +implements all standard methods
    +findById with lock
    +*contract for: init method for model binding
    *init method should add relations queries for each model
    +*contract for: create method
    +*contract for: delete method
    
PlaceRepo
    
    +ok

EventRepo
    
    +ok

VisitorRepo
    
    +ok
    
SeatRepo
    
    +filterByEvent
    +filterAvailable
    +filterRowNumber
    +filterSeatNumber

ImageRepo
    
    +ok

### Contract binding tests

+repositories

services

### Services

PlaceListService
    
    + all places (non paginated, always all or all in radius?)
    + has events list information

SeatListService
    
    +all seats for event
    +cannot list seats for deleted event, throw exception

VisitorCreateService
    
    +createForSeat

ImageSaveService
    
    +todo test actual file is stored...
    +saveForVisitor
    +save image from file
    +link to Visitor

SeatBookService
    
    +(DB transaction)
    +Lock seat, validate if seat is free 
    +(must allow concurency, i.e. block table for transaction)
    +create Visitor using VisitorCreateService
    +attach Image to Visitor with ImageSaveService
    +bookFor(Visitor) => set booked_at, link visitor
    +try to book a seat for Visitor model 
    +exception if seat is taken
    +should fire event SeatBookedEvent

### API

GET /api/v1/places
    
    +all places with events

GET /api/v1/events/:event/seats
    
    +get event => seats => contact's images
    +convert view prices with accuracy
    +404 if event not found or deleted

POST /api/v1/seats/:seat => SeatListResource
    
    +request: validate visitor info
    +request: upload image
    +request: check seat for availability (rule)
    +book event seat
    +try/catch bookFor for DB transaction errors 
    
### Seeders

+todo seeders for demo app

+factories

### Frontend work

EventsMap
    
    +get places with events and render
    
EventsSeats
    
    +render all event seats

BookSeat
    
    +form
    +validation
    =return to EventSeats and refresh data



>1) В одном месте могут проходить несколько мероприятий в разные даты?
>    1а) Если да, то нужно ли показывть "прошедшие" мероприятия, скажем в виде disabled кнопок с датами
В рамках тестового задания скажем, что нет, дабы упростить логику.

>2) "Информация о месте" мероприятия предполагает что-то вроде адреса, телефона например? т.е. это статичный объект с названием, адресом, координатами, верно? набор полей для демо выбираю >самостоятельно, помимо необходимых из логики проекта.

>1) Мы не делаем регистрацию или авторизацию участников, верно?
Нет, регистрацию мы не делаем.

>2) Исходя из п.1 Если участник бронирует место на имя компании которое уже есть в списках, какие наши действия? Пропускаем и обновлем лого, запрещаем такой ввод или пропускаем, но обновление лого игнорируем?
Если бронируется место с таким же именем мы не дожны менять лого у уже забронированной, так как это может быть другая компания. Одна компания может заброниромать несколько стендов.

>3) Если одна компания и разные ФИО, одинаковые ФИО, или разные ФИО и одинаковые емейлы - мы должны как-то с этим дублирванием бороться, отказывать в записии, например. Или у нас можно любое место на кого угодно забронировать, несмотря на дуликаты по ФИО, емейлам, телефонам, компаниям?
Да, можно. Но вопрос валидный, если бы это был реальный проект, я думаю, что логика была бы сложнее. 

>4) Бронируем по 1 месту за раз, верно?
Да, все верно.

>Тут есть несколько вариантов по сложности реализации. У кинозала схема статичная будет, у ярмарки/ресторана она может быть динамичная и очень сложная.
Схема на каждом мероприятии может быть своя, где то помещение больше и больше будет мест для бронирования, где то меньше. В рамках тестового это может быть мастрица, по разному заполненная для каждого мероприятия. То есть условно это квадратная территория с разным количеством мест для бронирования.




>Вопросы по заданию:
 
 Бронирование мест
 1) Мы не делаем регистрацию или авторизацию участников, верно?
 2) Исходя из п.1 Если участник бронирует место на имя компании которое уже есть в списках, какие наши действия? Пропускаем и обновлем лого, запрещаем такой ввод или пропускаем, но обновление лого игнорируем?
 3) Если одна компания и разные ФИО, одинаковые ФИО, или разные ФИО и одинаковые емейлы - мы должны как-то с этим дублирванием бороться, отказывать в записии, например. Или у нас можно любое место на кого угодно забронировать, несмотря на дуликаты по ФИО, емейлам, телефонам, компаниям?
 4) Бронируем по 1 месту за раз, верно?
 
 Мероприятия
 1) В одном месте могут проходить несколько мероприятий в разные даты?
 1а) Есил да, то нужно ли показывть "прошедшие" мероприятия, скажем в виде disabled кнопок с датами
 2) "Информация о месте" мероприятия предполагает что-то вроде адреса, телефона например? т.е. это статичный объект с названием, адресом, координатами, верно?
 2а) Мероприятия проводятся в этом объекте (связаны с этим местом), верно?
 
 Места рассадки
 Тут есть несколько вариантов по сложности реализации. У кинозала схема статичная будет, у ярмарки/ресторана она может быть динамичная и очень сложная.
 
 Я вижу такие варианты
 1) схема привязана к "Месту" (одна схема, копируется в Мероприятие, простая матрица, напр. 3х4, менять на мероприятии нельзя)
 2) схема каждого "Мероприятия" своя (задается в момент создания, матрицей вроде 3х4, без сложностей с боковыми местами, балконами и т.д.)
 3) базовая схема у "Места", копия в "Мероприятие" и еще возможность изменения/добавления/удаления мест, т.е. помимо рядов/мест добавляем еще сектора.
 
 Я предлагаю делать (1) или (2) :)
