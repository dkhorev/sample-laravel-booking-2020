<?php

use App\Http\Controllers\EventsController;
use App\Http\Controllers\PlacesController;
use App\Http\Controllers\SeatsController;

Route::group([
    'prefix' => 'v1/places',
], function () {
    Route::get('/', [PlacesController::class, 'index'])->name('places.index');
});

Route::group([
    'prefix' => 'v1/events',
], function () {
    Route::get('/{event}/seats', [EventsController::class, 'indexSeats'])->name('events.seats');
});

Route::group([
    'prefix' => 'v1/seats',
], function () {
    Route::post('/{seat}', [SeatsController::class, 'bookSeat'])->name('seats.book.one');
});
