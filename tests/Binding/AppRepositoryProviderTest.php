<?php

namespace Tests\Binding;

use App\Contracts\Repositories\EventRepoContract;
use App\Contracts\Repositories\ImageRepoContract;
use App\Contracts\Repositories\PlaceRepoContract;
use App\Contracts\Repositories\SeatRepoContract;
use App\Contracts\Repositories\VisitorRepoContract;
use App\Repositories\EventRepo;
use App\Repositories\ImageRepo;
use App\Repositories\PlaceRepo;
use App\Repositories\SeatRepo;
use App\Repositories\VisitorRepo;
use Tests\TestCase;

class AppRepositoryProviderTest extends TestCase
{
    /**
     * @return array
     */
    public function repoProvider()
    {
        return [
            'Event repo binding'   => [EventRepoContract::class, EventRepo::class],
            'Place repo binding'   => [PlaceRepoContract::class, PlaceRepo::class],
            'Visitor repo binding' => [VisitorRepoContract::class, VisitorRepo::class],
            'Seat repo binding'    => [SeatRepoContract::class, SeatRepo::class],
            'Image repo binding'   => [ImageRepoContract::class, ImageRepo::class],
        ];
    }

    /**
     * @test
     * @dataProvider repoProvider
     *
     * @param string $contract
     * @param string $class
     */
    public function testRepoBindings(string $contract, string $class)
    {
        // act
        $repo = app($contract);

        // assert
        $this->assertInstanceOf($contract, $repo);
        $this->assertInstanceOf($class, $repo);
    }
}
