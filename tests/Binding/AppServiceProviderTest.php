<?php

namespace Tests\Binding;

use App\Contracts\Services\ImageSaveServiceContract;
use App\Contracts\Services\SeatBookServiceContract;
use App\Contracts\Services\VisitorCreateServiceContract;
use App\Contracts\Services\PlaceListServiceContract;
use App\Contracts\Services\SeatListServiceContract;
use App\Services\ImageSaveService;
use App\Services\SeatBookService;
use App\Services\VisitorCreateService;
use App\Services\PlaceListService;
use App\Services\SeatListService;
use Tests\TestCase;

class AppServiceProviderTest extends TestCase
{
    /**
     * @return array
     */
    public function serviceProvider()
    {
        return [
            'PlaceListService binding'     => [PlaceListServiceContract::class, PlaceListService::class],
            'SeatListService binding'     => [SeatListServiceContract::class, SeatListService::class],
            'VisitorCreateService binding'     => [VisitorCreateServiceContract::class, VisitorCreateService::class],
            'ImageSaveService binding'     => [ImageSaveServiceContract::class, ImageSaveService::class],
            'SeatBookService binding'     => [SeatBookServiceContract::class, SeatBookService::class],
        ];
    }

    /**
     * @test
     * @dataProvider serviceProvider
     *
     * @param string $contract
     * @param string $class
     */
    public function testServiceBindings(string $contract, string $class)
    {
        // act
        $repo = app($contract);

        // assert
        $this->assertInstanceOf($contract, $repo);
        $this->assertInstanceOf($class, $repo);
    }
}
