<?php

namespace Tests\Unit\Models;

use App\Models\Event;
use App\Models\Seat;
use App\Models\Place;
use App\Models\Visitor;
use Exception;
use Tests\TestCase;

class EventTest extends TestCase
{
    /** @test */
    public function testModelFields()
    {
        /** @var Event $model */
        $model = factory(Event::class)->create()->fresh();

        $this->assertNotNull($model->id);
        $this->assertNotNull($model->name);
        $this->assertNotNull($model->place_id);
        $this->assertNotNull($model->starts_at);
        $this->assertNotNull($model->place);
        $this->assertNull($model->finished_at);
    }

    /** @test */
    public function testFillableParams()
    {
        // setup
        $place = factory(Place::class)->create();

        // act
        $date = now()->subDay()->startOfDay();
        $model = Event::create([
            'name'      => 'Bob\'s event',
            'place_id'  => $place->id,
            'starts_at' => $date,
        ]);

        // assert
        $this->assertEquals('Bob\'s event', $model->name);
        $this->assertEquals($place->id, $model->place_id);
        $this->assertEquals($date, $model->starts_at);
        $this->assertEquals(null, $model->finished_at);
    }

    /**
     * @test
     * @throws Exception
     */
    public function testSoftDeletes()
    {
        // setup
        /** @var Event $model */
        $model = factory(Event::class)->create();

        // pre assert
        $this->assertNull($model->finished_at);

        // act
        $model->delete();

        // assert
        $modelNew = Event::withTrashed()->first();
        $this->assertNotNull($modelNew->finished_at);
        $this->assertEquals($model->id, $modelNew->id);
    }

    /** @test */
    public function testPlaceRelation()
    {
        // setup
        /** @var Place $place1 */
        $place1 = factory(Place::class)->create();
        /** @var Place $place2 */
        $place2 = factory(Place::class)->create();

        /** @var Event $event1 */
        $event1 = factory(Event::class)->create(['place_id' => $place1]);
        /** @var Event $event2 */
        $event2 = factory(Event::class)->create(['place_id' => $place2]);

        // assert
        $this->assertCount(2, Place::all());
        $this->assertCount(2, Event::all());
        $this->assertEquals($event1->place->id, $place1->id);
        $this->assertEquals($event2->place->id, $place2->id);
    }

    /** @test */
    public function testEventSeatRelation()
    {
        // setup
        /** @var Event $event */
        $event = factory(Event::class)->create();
        $seat1 = factory(Seat::class)->create();
        $seat2 = factory(Seat::class)->create();
        $event->seats()->save($seat1);
        $event->seats()->save($seat2);
        factory(Seat::class, 5)->create();

        // assert
        $result = $event->seats->sortByDesc('id');
        $this->assertEquals(2, $result->count());
        $this->assertEquals($seat2->id, $result->first()->id);
        $this->assertEquals($seat1->id, $result->last()->id);
    }

    /** @test */
    public function testVisitorRelation()
    {
        // setup
        /** @var Event $event */
        $event = factory(Event::class)->create();
        $seat1 = factory(Seat::class)->create();
        $seat2 = factory(Seat::class)->create();
        $seat3 = factory(Seat::class)->create();
        $event->seats()->save($seat1);
        $event->seats()->save($seat2);
        $event->seats()->save($seat3);
        factory(Seat::class, 5)->create();
        // visitors
        $visitor1 = factory(Visitor::class)->create(['seat_id' => $seat1]);
        $visitor2 = factory(Visitor::class)->create(['seat_id' => $seat2]);

        // assert
        $result = $event->visitors->sortBy('id');
        $this->assertEquals(2, $result->count());
        $this->assertEquals($visitor1->id, $result->first()->id);
        $this->assertEquals($visitor2->id, $result->last()->id);
    }
}
