<?php

namespace Tests\Unit\Models;

use App\Models\Event;
use App\Models\Seat;
use App\Models\Visitor;
use Illuminate\Database\QueryException;
use Tests\TestCase;

class SeatTest extends TestCase
{
    /** @test */
    public function testModelFields()
    {
        /** @var Seat $model */
        $model = factory(Seat::class)->create()->fresh();

        $this->assertNotNull($model->id);
        $this->assertNotNull($model->event_id);
        $this->assertNotNull($model->row_number);
        $this->assertNotNull($model->seat_number);
        $this->assertNotNull($model->price);
        $this->assertNotNull($model->event);
        $this->assertNull($model->booked_at);
    }

    /** @test */
    public function testFillableParams()
    {
        // setup
        $event = factory(Event::class)->create();

        // act
        $date = now()->subDay()->startOfDay();
        /** @var Seat $model */
        $model = Seat::create([
            'event_id'    => $event->id,
            'row_number'  => 1,
            'seat_number' => 11,
            'price'       => 100,
            'booked_at'   => $date,
        ]);

        // assert
        $this->assertEquals($event->id, $model->event_id);
        $this->assertEquals($date, $model->booked_at);
        $this->assertEquals(1, $model->row_number);
        $this->assertEquals(11, $model->seat_number);
        $this->assertEquals(100, $model->price);
    }

    /** @test */
    public function testEventRelation()
    {
        // setup
        /** @var Event $event1 */
        $event1 = factory(Event::class)->create();
        /** @var Event $event2 */
        $event2 = factory(Event::class)->create();

        /** @var Seat $seat1 */
        $seat1 = factory(Seat::class)->create(['event_id' => $event1]);
        /** @var Seat $seat2 */
        $seat2 = factory(Seat::class)->create(['event_id' => $event2]);

        // assert
        $this->assertCount(2, Event::all());
        $this->assertCount(2, Seat::all());
        $this->assertEquals($seat1->event->id, $event1->id);
        $this->assertEquals($seat2->event->id, $event2->id);
    }

    /** @test */
    public function testVisitorRelation()
    {
        /** @var Seat $seat1 */
        $seat1 = factory(Seat::class)->create();
        /** @var Seat $seat2 */
        $seat2 = factory(Seat::class)->create();

        /** @var Visitor $visitor1 */
        $visitor1 = factory(Visitor::class)->create(['seat_id' => $seat1]);
        /** @var Visitor $visitor2 */
        $visitor2 = factory(Visitor::class)->create(['seat_id' => $seat2]);

        // assert
        $this->assertCount(2, Visitor::all());
        $this->assertCount(2, Seat::all());
        $this->assertEquals($visitor1->seat->id, $seat1->id);
        $this->assertEquals($visitor2->seat->id, $seat2->id);
    }

    /** @test */
    public function testUniqueEventAndRowAndSeat()
    {
        // setup
        /** @var Seat $seat */
        $seat = factory(Seat::class)->create([
            'row_number'  => 1,
            'seat_number' => 1,
        ]);

        // assert
        $this->expectException(QueryException::class);

        // act
        factory(Seat::class)->create([
            'row_number'  => 1,
            'seat_number' => 1,
            'event_id'    => $seat->event->id,
        ]);
    }
}
