<?php

namespace Tests\Unit\Models;

use App\Models\Image;
use App\Models\Seat;
use App\Models\Visitor;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class VisitorTest extends TestCase
{
    /** @test */
    public function testModelFields()
    {
        /** @var Visitor $model */
        $model = factory(Visitor::class)->create()->fresh();

        $this->assertNotNull($model->id);
        $this->assertNotNull($model->name);
        $this->assertNotNull($model->company);
        $this->assertNotNull($model->email);
        $this->assertNotNull($model->phone);
        $this->assertNotNull($model->seat_id);
        $this->assertNotNull($model->seat);
    }

    /** @test */
    public function testFillableParams()
    {
        // setup
        $seat = factory(Seat::class)->create();

        // act
        /** @var Visitor $model */
        $model = Visitor::create([
            'name'    => 'Bob Joe',
            'company' => 'Bob\'s company',
            'email'   => 'bob@email.com',
            'phone'   => '+7888',
            'desc'    => 'my desc',
            'seat_id' => $seat->id,
        ]);

        // assert
        $this->assertEquals($seat->id, $model->seat_id);
        $this->assertEquals($seat->id, $model->seat->id);
        $this->assertEquals('Bob Joe', $model->name);
        $this->assertEquals('Bob\'s company', $model->company);
        $this->assertEquals('bob@email.com', $model->email);
        $this->assertEquals('+7888', $model->phone);
        $this->assertEquals('my desc', $model->desc);
    }

    /** @test */
    public function testSeatRelation()
    {
        // setup
        /** @var Seat $event1 */
        $seat1 = factory(Seat::class)->create();
        /** @var Seat $seat2 */
        $seat2 = factory(Seat::class)->create();

        /** @var Visitor $visitor1 */
        $visitor1 = factory(Visitor::class)->create(['seat_id' => $seat1]);
        /** @var Visitor $visitor2 */
        $visitor2 = factory(Visitor::class)->create(['seat_id' => $seat2]);

        // assert
        $this->assertCount(2, Seat::all());
        $this->assertCount(2, Visitor::all());
        $this->assertEquals($visitor1->seat->id, $seat1->id);
        $this->assertEquals($visitor2->seat->id, $seat2->id);
    }

    /** @test */
    public function testImageRelation()
    {
        // setup
        Storage::fake('public');
        $image = UploadedFile::fake()->image('photo1.jpg');
        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->create();
        $visitor->image()->save(factory(Image::class)->create([
            'filename' => $image->getClientOriginalName(),
            'mime'     => $image->getMimeType(),
            'ext'      => $image->getClientOriginalExtension(),
            'size'     => $image->getSize(),
        ]));

        // assert
        $this->assertNotNull($visitor->image);
        $this->assertEquals($image->getClientOriginalName(), $visitor->image->filename);
        $this->assertEquals($image->getMimeType(), $visitor->image->mime);
        $this->assertEquals($image->getClientOriginalExtension(), $visitor->image->ext);
        $this->assertEquals($image->getSize(), $visitor->image->size);
    }
}
