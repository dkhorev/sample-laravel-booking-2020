<?php

namespace Tests\Unit\Models;

use App\Models\Image;
use App\Models\Visitor;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ImageTest extends TestCase
{
    /** @test */
    public function testModelFieldsDefault()
    {
        /** @var Image $model */
        $model = factory(Image::class)->create()->fresh();

        $this->assertNotNull($model->id);
        $this->assertNull($model->filename);
        $this->assertNull($model->mime);
        $this->assertNull($model->ext);
        $this->assertNull($model->size);
        $this->assertNotNull($model->visitor);
        $this->assertNotNull($model->visitor_id);
    }

    /** @test */
    public function testModelWithFile()
    {
        // setup
        Storage::fake('public');
        $image = UploadedFile::fake()->image('photo1.jpg');

        /** @var Image $model */
        $model = factory(Image::class)->create([
            'filename' => $image->getClientOriginalName(),
            'mime'     => $image->getMimeType(),
            'ext'      => $image->getClientOriginalExtension(),
            'size'     => $image->getSize(),
        ]);

        // asserts
        $model = $model->fresh();
        $this->assertNotNull($model->id);
        $this->assertNotNull($model->filename);
        $this->assertNotNull($model->mime);
        $this->assertNotNull($model->ext);
        $this->assertNotNull($model->size);
        $this->assertNotNull($model->visitor);
        $this->assertNotNull($model->visitor_id);
        $this->assertEquals('photo1.jpg', $model->filename);
        $this->assertEquals($image->getMimeType(), $model->mime);
        $this->assertEquals($image->getClientOriginalExtension(), $model->ext);
        $this->assertEquals($image->getSize(), $model->size);
    }

    /** @test */
    public function testFillableParams()
    {
        // setup
        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->create();

        // act
        /** @var Image $model */
        $model = Image::create([
            'filename'   => 'photo2.jpg',
            'mime'       => 'jpeg|jpg',
            'ext'        => 'jpg',
            'size'       => '1234',
            'visitor_id' => $visitor->id,
        ]);

        // assert
        $this->assertEquals($visitor->id, $model->visitor_id);
        $this->assertEquals($visitor->id, $model->visitor->id);
        $this->assertEquals('photo2.jpg', $model->filename);
        $this->assertEquals('jpeg|jpg', $model->mime);
        $this->assertEquals('jpg', $model->ext);
        $this->assertEquals('1234', $model->size);
    }

    /** @test */
    public function testVisitorRelation()
    {
        // setup
        /** @var Visitor $event1 */
        $visitor1 = factory(Visitor::class)->create();
        /** @var Visitor $visitor2 */
        $visitor2 = factory(Visitor::class)->create();

        /** @var Image $image1 */
        $image1 = factory(Image::class)->create(['visitor_id' => $visitor1]);
        /** @var Image $image2 */
        $image2 = factory(Image::class)->create(['visitor_id' => $visitor2]);

        // assert
        $this->assertCount(2, Visitor::all());
        $this->assertCount(2, Image::all());
        $this->assertEquals($image1->visitor->id, $visitor1->id);
        $this->assertEquals($image2->visitor->id, $visitor2->id);
    }
}
