<?php

namespace Tests\Unit\Models;

use App\Models\Event;
use App\Models\Place;
use Tests\TestCase;

class PlaceTest extends TestCase
{
    /** @test */
    public function testModelFields()
    {
        /** @var Place $model */
        $model = factory(Place::class)->create()->fresh();

        $this->assertNotNull($model->id);
        $this->assertNotNull($model->name);
        $this->assertNotNull($model->address);
        $this->assertNotNull($model->phone);
        $this->assertNotNull($model->lon);
        $this->assertNotNull($model->lat);
        $this->assertNotNull($model->created_at);
        $this->assertNotNull($model->events);
    }

    /** @test */
    public function testFillableParams()
    {
        // act
        $date = now()->subDay()->startOfDay();
        $model = Place::create([
            'name'       => 'Bob\'s place',
            'address'    => 'Some address',
            'phone'      => '+78888',
            'lon'        => 40.5,
            'lat'        => 0.5,
            'created_at' => $date,
        ]);

        // assert
        $this->assertEquals('Bob\'s place', $model->name);
        $this->assertEquals('Some address', $model->address);
        $this->assertEquals('+78888', $model->phone);
        $this->assertEquals(40.5, $model->lon);
        $this->assertEquals(0.5, $model->lat);
        $this->assertEquals($date, $model->created_at);
    }

    /** @test */
    public function testEventsRelation()
    {
        // setup
        /** @var Place $model */
        $model = factory(Place::class)->create();
        $event1 = factory(Event::class)->create();
        $event2 = factory(Event::class)->create();
        $model->events()->save($event1);
        $model->events()->save($event2);

        // assert
        $result = $model->events;
        $this->assertEquals(2, $result->count());
        $this->assertEquals($event1->id, $result->first()->id);
        $this->assertEquals($event2->id, $result->last()->id);
    }
}
