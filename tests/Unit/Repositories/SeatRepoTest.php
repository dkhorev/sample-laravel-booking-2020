<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\SeatRepoContract;
use App\Models\Event;
use App\Models\Seat;
use App\Repositories\SeatRepo;
use Tests\TestCase;

class SeatRepoTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var SeatRepoContract $repo */
        $repo = app(SeatRepo::class);

        $this->assertInstanceOf(SeatRepo::class, $repo);
        $this->assertInstanceOf(SeatRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(Seat::class, 3)->create();

        // act
        /** @var SeatRepo $repo */
        $repo = app(SeatRepo::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(Seat::class, $model);
        }
    }

    /** @test */
    public function testFilterByEventId()
    {
        // setup
        $event = factory(Event::class)->create();
        factory(Seat::class, 3)->create();
        $seatExpected = factory(Seat::class)->create(['event_id' => $event]);

        // act
        /** @var SeatRepoContract $repo */
        $repo = app(SeatRepo::class);
        $seat = $repo->filterByEventId($event->id)->get();

        // assert
        $this->assertCount(1, $seat);
        $this->assertEquals($seatExpected->id, $seat->first()->id);
    }

    /** @test */
    public function testFilterAvailable()
    {
        factory(Seat::class, 3)->create(['booked_at' => now()]);
        $seatExpected = factory(Seat::class)->create(['booked_at' => null]);

        // act
        /** @var SeatRepoContract $repo */
        $repo = app(SeatRepo::class);
        $seat = $repo->filterAvailable()->get();

        // assert
        $this->assertCount(1, $seat);
        $this->assertEquals($seatExpected->id, $seat->first()->id);
    }

    /** @test */
    public function testFilterRowNumber()
    {
        factory(Seat::class, 3)->create(['row_number' => 1]);
        $seatExpected1 = factory(Seat::class)->create(['row_number' => 2]);
        $seatExpected2 = factory(Seat::class)->create(['row_number' => 2]);

        // act
        /** @var SeatRepoContract $repo */
        $repo = app(SeatRepo::class);
        $seats = $repo->filterRowNumber(2)->get()->sortBy('id');

        // assert
        $this->assertCount(2, $seats);
        $this->assertEquals($seatExpected1->id, $seats->first()->id);
        $this->assertEquals($seatExpected2->id, $seats->last()->id);
    }

    /** @test */
    public function testFilterSeatNumber()
    {
        factory(Seat::class, 3)->create(['seat_number' => 1]);
        $seatExpected1 = factory(Seat::class)->create(['seat_number' => 2]);
        factory(Seat::class)->create(['seat_number' => 3]);

        // act
        /** @var SeatRepoContract $repo */
        $repo = app(SeatRepo::class);
        $seats = $repo->filterSeatNumber(2)->get();

        // assert
        $this->assertCount(1, $seats);
        $this->assertEquals($seatExpected1->id, $seats->first()->id);
    }
}
