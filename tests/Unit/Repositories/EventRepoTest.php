<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\EventRepoContract;
use App\Models\Event;
use App\Repositories\EventRepo;
use Tests\TestCase;

class EventRepoTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var EventRepo $repo */
        $repo = app(EventRepo::class);

        $this->assertInstanceOf(EventRepo::class, $repo);
        $this->assertInstanceOf(EventRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(Event::class, 3)->create();

        // act
        /** @var EventRepo $repo */
        $repo = app(EventRepo::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(Event::class, $model);
        }
    }
}
