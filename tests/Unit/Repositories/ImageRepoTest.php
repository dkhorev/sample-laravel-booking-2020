<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\ImageRepoContract;
use App\Models\Image;
use App\Repositories\ImageRepo;
use Tests\TestCase;

class ImageRepoTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var ImageRepo $repo */
        $repo = app(ImageRepo::class);

        $this->assertInstanceOf(ImageRepo::class, $repo);
        $this->assertInstanceOf(ImageRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(Image::class, 3)->create();

        // act
        /** @var ImageRepo $repo */
        $repo = app(ImageRepo::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(Image::class, $model);
        }
    }
}
