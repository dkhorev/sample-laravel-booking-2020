<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\PlaceRepoContract;
use App\Models\Place;
use App\Repositories\PlaceRepo;
use Tests\TestCase;

class PlaceRepoTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var PlaceRepo $repo */
        $repo = app(PlaceRepo::class);

        $this->assertInstanceOf(PlaceRepo::class, $repo);
        $this->assertInstanceOf(PlaceRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(Place::class, 3)->create();

        // act
        /** @var PlaceRepo $repo */
        $repo = app(PlaceRepo::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(Place::class, $model);
        }
    }
}
