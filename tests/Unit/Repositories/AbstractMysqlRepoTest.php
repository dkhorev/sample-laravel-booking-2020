<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\RepoContract;
use App\Models\Place;
use App\Repositories\PlaceRepo;
use Exception;
use Tests\TestCase;

class AbstractMysqlRepoTest extends TestCase
{
    const TEST_CLASS = Place::class;
    const TEST_REPO  = PlaceRepo::class;

    /**
     * @var RepoContract
     */
    protected $repo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repo = app(static::TEST_REPO);
    }

    /** @test */
    public function testCreate()
    {
        // setup
        $model = factory(static::TEST_CLASS)->make()->toArray();

        // act
        /** @var Place $result */
        $result = $this->repo->create($model);

        // assert
        $this->assertEquals($model['name'], $result->name);
    }

    /**
     * @test
     * @throws Exception
     */
    public function testDelete()
    {
        // setup
        $model = factory(static::TEST_CLASS)->create();

        // pre assert
        $this->assertEquals(1, app(static::TEST_CLASS)->count());

        // act
        $this->repo->delete($model);

        // pre assert
        $this->assertEquals(0, app(static::TEST_CLASS)->count());
    }

    /** @test */
    public function testFindById()
    {
        // setup
        $model = factory(static::TEST_CLASS)->create();
        factory(static::TEST_CLASS, 5)->create();

        // act
        /** @var Place $result */
        $result = $this->repo->findById($model->id);

        // assert
        $this->assertInstanceOf(static::TEST_CLASS, $model);
        $this->assertEquals($result->id, $model->id);
    }
}
