<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\VisitorRepoContract;
use App\Models\Visitor;
use App\Repositories\VisitorRepo;
use Tests\TestCase;

class VisitorRepoTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var VisitorRepo $repo */
        $repo = app(VisitorRepo::class);

        $this->assertInstanceOf(VisitorRepo::class, $repo);
        $this->assertInstanceOf(VisitorRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(Visitor::class, 3)->create();

        // act
        /** @var VisitorRepo $repo */
        $repo = app(VisitorRepo::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(Visitor::class, $model);
        }
    }
}
