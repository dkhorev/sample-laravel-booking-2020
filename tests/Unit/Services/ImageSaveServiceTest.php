<?php

namespace Tests\Unit\Services;

use App\Contracts\Services\ImageSaveServiceContract;
use App\Models\Image;
use App\Models\Visitor;
use App\Services\ImageSaveService;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ImageSaveServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var ImageSaveServiceContract $service */
        $service = app(ImageSaveService::class);

        $this->assertInstanceOf(ImageSaveService::class, $service);
        $this->assertInstanceOf(ImageSaveServiceContract::class, $service);
    }

    /** @test */
    public function testSave()
    {
        // setup
        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->create();
        Storage::fake('public');
        $image = UploadedFile::fake()->image('photo1.jpg');

        // act
        /** @var ImageSaveServiceContract $service */
        $service = app(ImageSaveService::class);
        $result = $service->saveForVisitor($image, $visitor->id);

        // assert
        $this->assertCount(1, Image::all());
        $this->assertEquals($image->getClientOriginalName(), $result->filename);
        $this->assertEquals($image->getMimeType(), $result->mime);
        $this->assertEquals($image->getClientOriginalExtension(), $result->ext);
        $this->assertEquals($image->getSize(), $result->size);
        $this->assertEquals($visitor->id, $result->visitor_id);
        $this->assertEquals($visitor->id, $result->visitor->id);

        // assert files save
        Storage::disk('public')->assertExists($result->getStoragePath());
    }

    /** @test */
    public function testExceptionWhenNoVisitor()
    {
        // setup
        Storage::fake('public');
        $image = UploadedFile::fake()->image('photo1.jpg');

        // assert
        $this->expectException(QueryException::class);

        // act
        /** @var ImageSaveServiceContract $service */
        $service = app(ImageSaveService::class);
        $service->saveForVisitor($image, 9999);
    }
}
