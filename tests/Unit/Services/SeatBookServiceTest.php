<?php

namespace Tests\Unit\Services;

use App\Contracts\Services\SeatBookServiceContract;
use App\Events\Services\SeatBookedEvent;
use App\Exceptions\SeatBookServiceException;
use App\Exceptions\SeatNotAvailableException;
use App\Models\Event;
use App\Models\Image;
use App\Models\Seat;
use App\Models\Visitor;
use App\Services\SeatBookService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Throwable;

class SeatBookServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var SeatBookServiceContract $service */
        $service = app(SeatBookService::class);

        $this->assertInstanceOf(SeatBookService::class, $service);
        $this->assertInstanceOf(SeatBookServiceContract::class, $service);
    }

    /**
     * @test
     * @throws SeatNotAvailableException
     * @throws SeatBookServiceException
     * @throws Throwable
     */
    public function testBooksCorrectly()
    {
        // setup
        \Illuminate\Support\Facades\Event::fake();
        Storage::fake('public');
        $image = UploadedFile::fake()->image('photo1.jpg');
        /** @var Visitor $expectedVisitor */
        $expectedVisitor = factory(Visitor::class)->make();
        $seat = $expectedVisitor->seat;
        $event = $seat->event;

        // pre assert
        $this->assertCount(0, Visitor::all());
        $this->assertCount(1, Seat::all());
        $this->assertCount(1, Event::all());
        $this->assertCount(0, Image::all());
        $this->assertNull($seat->booked_at);

        // act
        /** @var SeatBookServiceContract $service */
        $service = app(SeatBookService::class);
        $result = $service->setSeat($seat->id)
                          ->setImage($image)
                          ->bookFor($expectedVisitor->toArray());

        // assert
        $this->assertEquals($seat->id, $result->id);
        $this->assertEquals($event->id, $result->event->id);
        $this->assertNotNull($result->booked_at);
        $this->assertCount(1, Visitor::all());
        $visitor = Visitor::first();
        $this->assertEquals($expectedVisitor->name, $visitor->name);
        $this->assertEquals($event->id, $visitor->seat->event->id);
        $this->assertEquals($seat->id, $visitor->seat->id);
        $this->assertCount(1, Image::all());
        $image = Image::first();
        $this->assertEquals($visitor->id, $image->visitor_id);
        // assert events
        \Illuminate\Support\Facades\Event::assertDispatched(SeatBookedEvent::class);
    }

    /**
     * @test
     * @throws SeatBookServiceException
     * @throws Throwable
     */
    public function testExceptionIfSeatNotSet()
    {
        // setup
        Storage::fake('public');
        $image = UploadedFile::fake()->image('photo1.jpg');

        // assert
        $this->expectException(SeatBookServiceException::class);

        // act
        /** @var SeatBookServiceContract $service */
        $service = app(SeatBookService::class);
        $service->setImage($image)
                ->bookFor([]);
    }

    /**
     * @test
     * @throws SeatNotAvailableException
     * @throws SeatBookServiceException
     * @throws Throwable
     */
    public function testExceptionIfImageNotSet()
    {
        /** @var Seat $seat */
        $seat = factory(Seat::class)->create();

        // assert
        $this->expectException(SeatBookServiceException::class);

        // act
        /** @var SeatBookServiceContract $service */
        $service = app(SeatBookService::class);
        $service->setSeat($seat->id)
                ->bookFor([]);
    }

    /**
     * @test
     * @throws SeatNotAvailableException
     * @throws SeatBookServiceException
     * @throws Throwable
     */
    public function testExceptionIfSeatIsBusy()
    {
        // setup
        /** @var Seat $seat */
        $seat = factory(Seat::class)->create(['booked_at' => now()]);

        // assert
        $this->expectException(SeatNotAvailableException::class);

        // act
        /** @var SeatBookServiceContract $service */
        $service = app(SeatBookService::class);
        $service->setSeat($seat->id)
                ->bookFor([]);
    }

    /**
     * @test
     * @throws SeatNotAvailableException
     * @throws SeatBookServiceException
     * @throws Throwable
     */
    public function testExceptionIfNoSeat()
    {
        // assert
        $this->expectException(SeatNotAvailableException::class);

        // act
        /** @var SeatBookServiceContract $service */
        $service = app(SeatBookService::class);
        $service->setSeat(9999)
                ->bookFor([]);
    }
}
