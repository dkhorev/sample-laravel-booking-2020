<?php

namespace Tests\Unit\Services;

use App\Contracts\Services\PlaceListServiceContract;
use App\Models\Event;
use App\Models\Place;
use App\Services\PlaceListService;
use Tests\TestCase;

class PlaceListServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var PlaceListServiceContract $service */
        $service = app(PlaceListService::class);

        $this->assertInstanceOf(PlaceListService::class, $service);
        $this->assertInstanceOf(PlaceListServiceContract::class, $service);
    }

    /** @test */
    public function testAll()
    {
        // setup
        factory(Event::class, 5)->create();

        // pre assert
        $this->assertCount(5, Place::all());

        // act
        /** @var PlaceListServiceContract $service */
        $service = app(PlaceListService::class);
        $result = $service->all();

        // assert
        $this->assertCount(5, $result);
        $result->each(function ($place) {
            $this->assertInstanceOf(Place::class, $place);
            $this->assertNotNull($place->events);
            $this->assertCount(1, $place->events);

            $place->events->each(function ($event) {
                $this->assertInstanceOf(Event::class, $event);
            });
        });
    }
}
