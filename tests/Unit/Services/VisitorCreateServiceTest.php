<?php

namespace Tests\Unit\Services;

use App\Contracts\Services\VisitorCreateServiceContract;
use App\Models\Seat;
use App\Models\Visitor;
use App\Services\VisitorCreateService;
use Illuminate\Database\QueryException;
use Tests\TestCase;

class VisitorCreateServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var VisitorCreateServiceContract $service */
        $service = app(VisitorCreateService::class);

        $this->assertInstanceOf(VisitorCreateService::class, $service);
        $this->assertInstanceOf(VisitorCreateServiceContract::class, $service);
    }

    /** @test */
    public function testCreate()
    {
        // setup
        /** @var Visitor $expected */
        $expected = factory(Visitor::class)->make();

        // act
        /** @var VisitorCreateServiceContract $service */
        $service = app(VisitorCreateService::class);
        $result = $service->createForSeat([
            'name'    => $expected->name,
            'company' => $expected->company,
            'email'   => $expected->email,
            'phone'   => $expected->phone,
        ], factory(Seat::class)->create()->id);

        // assert
        $this->assertCount(1, Visitor::all());
        $this->assertEquals($expected->name, $result->name);
        $this->assertEquals($expected->company, $result->company);
        $this->assertEquals($expected->email, $result->email);
        $this->assertEquals($expected->phone, $result->phone);
        $this->assertNotNull($result->seat);
    }

    /** @test */
    public function testWhenSeatNotExists()
    {
        // assert
        $this->expectException(QueryException::class);

        // act
        /** @var VisitorCreateServiceContract $service */
        $service = app(VisitorCreateService::class);
        $service->createForSeat([
            'name'    => '123',
            'company' => '123',
            'email'   => '123',
            'phone'   => '123',
        ], 999);
    }
}
