<?php

namespace Tests\Unit\Services;

use App\Contracts\Services\SeatListServiceContract;
use App\Exceptions\EventNotFoundException;
use App\Models\Event;
use App\Models\Seat;
use App\Services\SeatListService;
use Tests\TestCase;

class SeatListServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var SeatListServiceContract $service */
        $service = app(SeatListService::class);

        $this->assertInstanceOf(SeatListService::class, $service);
        $this->assertInstanceOf(SeatListServiceContract::class, $service);
    }

    /**
     * @test
     * @throws EventNotFoundException
     */
    public function testAll()
    {
        // setup
        /** @var Event $event1 */
        $event1 = factory(Event::class)->create();
        $event1->seats()->save(factory(Seat::class)->create(['price' => 100]));
        $event1->seats()->save(factory(Seat::class)->create(['price' => 100]));
        /** @var Event $eventExpected */
        $eventExpected = factory(Event::class)->create();
        $eventExpected->seats()->save(factory(Seat::class)->create(['price' => 999]));
        $eventExpected->seats()->save(factory(Seat::class)->create(['price' => 999]));
        $eventExpected->seats()->save(factory(Seat::class)->create(['price' => 999]));

        // act
        /** @var SeatListServiceContract $service */
        $service = app(SeatListService::class);
        $result = $service->allForEventId($eventExpected->id);

        // assert
        $this->assertCount(3, $result);
        $result->each(function (Seat $seat) {
            $this->assertEquals(999, $seat->price);
        });
    }

    /**
     * @test
     * @throws EventNotFoundException
     */
    public function testExceptionOnInvalidEventId()
    {
        // assert
        $this->expectException(EventNotFoundException::class);

        // act
        /** @var SeatListServiceContract $service */
        $service = app(SeatListService::class);
        $service->allForEventId(999);
    }
}
