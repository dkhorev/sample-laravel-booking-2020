<?php

namespace Tests\Feature\Requests;

use App\Models\Seat;
use App\Models\Visitor;
use Illuminate\Http\Response;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class SeatBookRequestTest extends TestCase
{
    const TEST_ROUTE = 'seats.book.one';

    /**
     * @var File
     */
    private $image;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('public');
        $this->image = UploadedFile::fake()->image('photo1.jpg')->size(1000);
    }

    public function invalidVisitorFieldProvider()
    {
        return [
            'no name'    => ['name'],
            'no company' => ['company'],
            'no email'   => ['email'],
            'no phone'   => ['phone'],
            'no image'   => ['image'],
        ];
    }

    /**
     * @test
     * @dataProvider invalidVisitorFieldProvider
     *
     * @param $field
     */
    public function testValidatesVisitorInfo($field)
    {
        // setup
        $seat = factory(Seat::class)->create();
        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->make();
        $data = [
            'name'    => $visitor->name,
            'company' => $visitor->company,
            'email'   => $visitor->email,
            'phone'   => $visitor->phone,
            'desc'    => $visitor->desc,
            'image'   => $this->image,
        ];
        unset($data[$field]);

        // act
        $respone = $this->json('post', route(static::TEST_ROUTE, ['seat' => $seat->id]), $data);

        // assert
        $respone->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function testHasEmailValidation()
    {
        // setup
        $seat = factory(Seat::class)->create();
        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->make();
        $data = [
            'email'   => 'bad-email',
            'name'    => $visitor->name,
            'company' => $visitor->company,
            'phone'   => $visitor->phone,
            'desc'    => $visitor->desc,
            'image'   => $this->image,
        ];

        // act
        $respone = $this->json('post', route(static::TEST_ROUTE, ['seat' => $seat->id]), $data);

        // assert
        $respone->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $result = $respone->decodeResponseJson();
        $this->assertArrayHasKey('email', $result['errors']);
        $this->assertCount(1, $result['errors']);
    }

    public function invalidPhoneProvider()
    {
        return [
            ['789'],
            [''],
            ['   '],
            [false],
            [null],
            [0],
            [1],
            '14 char phone' => ['01234567891112'],
            '7 char phone'  => ['0123456'],
        ];
    }

    /**
     * @test
     * @dataProvider invalidPhoneProvider
     *
     * @param $phone
     */
    public function testHasPhoneValidation($phone)
    {
        // setup
        $seat = factory(Seat::class)->create();
        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->make();
        $data = [
            'name'    => $visitor->name,
            'company' => $visitor->company,
            'email'   => $visitor->email,
            'phone'   => $phone,
            'desc'    => $visitor->desc,
            'image'   => $this->image,
        ];

        // act
        $respone = $this->json('post', route(static::TEST_ROUTE, ['seat' => $seat->id]), $data);

        // assert
        $respone->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $result = $respone->decodeResponseJson();
        $this->assertArrayHasKey('phone', $result['errors']);
        $this->assertCount(1, $result['errors']);
    }

    public function validPhoneProvider()
    {
        return [
            ['+7(999)888-77-66'],
            ['+79998887766'],
            ['79998887766'],
            ['+7(999)888-77-66'],
        ];
    }

    /**
     * @test
     * @dataProvider validPhoneProvider
     *
     * @param $phone
     */
    public function testAcceptsCorrectPhones($phone)
    {
        // setup
        $seat = factory(Seat::class)->create();
        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->make();
        $data = [
            'name'    => $visitor->name,
            'company' => $visitor->company,
            'email'   => $visitor->email,
            'phone'   => $phone,
            'desc'    => $visitor->desc,
            'image'   => $this->image,
        ];

        // act
        $respone = $this->json('post', route(static::TEST_ROUTE, ['seat' => $seat->id]), $data);

        // assert
        $respone->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function testCheckThasSeatIsNotBooked()
    {
        // setup
        /** @var Seat $seat */
        $seat = factory(Seat::class)->create(['booked_at' => now()]);

        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->make();
        $data = [
            'name'    => $visitor->name,
            'company' => $visitor->company,
            'email'   => $visitor->email,
            'phone'   => $visitor->phone,
            'desc'    => $visitor->desc,
            'image'   => $this->image,
        ];

        // act
        $respone = $this->json('post', route(static::TEST_ROUTE, ['seat' => $seat->id]), $data);

        // assert
        $respone->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $result = $respone->decodeResponseJson();
        $this->assertArrayHasKey('seat_id', $result['errors']);
        $this->assertCount(1, $result['errors']);
    }
}
