<?php

namespace Tests\Feature\Controllers;

use App\Models\Seat;
use App\Models\Visitor;
use Illuminate\Http\Response;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class SeatsControllerTest extends TestCase
{
    /**
     * @var File
     */
    private $image;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('public');
        $this->image = UploadedFile::fake()->image('photo1.jpg')->size(1000);
    }

    /** @test */
    public function test404OnInvalidSeat()
    {
        // act
        $responce = $this->json('post', route('seats.book.one', ['seat' => 999]));

        // assert
        $responce->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function testSeatBooked()
    {
        $this->withoutExceptionHandling();
        // setup
        $seat = factory(Seat::class)->create();
        /** @var Visitor $visitor */
        $visitor = factory(Visitor::class)->make();
        $data = [
            'name'    => $visitor->name,
            'company' => $visitor->company,
            'email'   => $visitor->email,
            'phone'   => $visitor->phone,
            'desc'    => $visitor->desc,
            'image'   => $this->image,
        ];

        // act
        $responce = $this->json('post', route('seats.book.one', ['seat' => $seat->id]), $data);

        // assert
        $responce->assertStatus(Response::HTTP_OK);
        $responce->assertJsonStructure([
            'data' => [
                'id',
                'row_number',
                'seat_number',
                'price',
                'is_booked',
                'visitor_logo',
            ],
        ]);
        // assert for models
        $result = $responce->decodeResponseJson();
        $this->assertNotNull($result['data']['is_booked']);
        $this->assertNotNull($result['data']['visitor_logo']);
        $this->assertCount(1, Visitor::all());
        $bookedVisitor = Visitor::first();
        $this->assertEquals($visitor->name, $bookedVisitor->name);
        $this->assertEquals($visitor->company, $bookedVisitor->company);
        $this->assertEquals($visitor->email, $bookedVisitor->email);
        $this->assertEquals($visitor->phone, $bookedVisitor->phone);
        $this->assertEquals($visitor->desc, $bookedVisitor->desc);
    }
}
