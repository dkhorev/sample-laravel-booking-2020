<?php

namespace Tests\Feature\Controllers;

use App\Contracts\Services\SeatBookServiceContract;
use App\Contracts\Services\SeatListServiceContract;
use App\Exceptions\SeatBookServiceException;
use App\Exceptions\SeatNotAvailableException;
use App\Models\Event;
use App\Models\Seat;
use App\Services\SeatBookService;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Mockery\MockInterface;
use Tests\TestCase;
use Throwable;

class EventsControllerTest extends TestCase
{
    /** @test */
    public function test404WhenNotFound()
    {
        // act
        $response = $this->json('get', route('events.seats', ['event' => 9999]));

        // assert
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function testSeatsUnbooked()
    {
        // setup
        /** @var Seat $seat1 */
        $seat1 = factory(Seat::class)->create();
        $event = $seat1->event;
        $event->seats()->save(factory(Seat::class)->create());
        $event->seats()->save(factory(Seat::class)->create());
        // other seats and events
        factory(Seat::class)->create();
        factory(Seat::class)->create();
        factory(Seat::class)->create();

        // act
        $response = $this->json('get', route('events.seats', ['event' => $event->id]));

        // assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'row_number',
                    'seat_number',
                    'price',
                    'is_booked',
                    'visitor_logo',
                ],
            ],
        ]);
        $result = $response->decodeResponseJson();
        $this->assertCount(3, $result['data']);
    }

    /**
     * @test
     * @throws SeatBookServiceException
     * @throws SeatNotAvailableException
     * @throws Throwable
     */
    public function testSeatsBooked()
    {
        // setup
        /** @var Seat $seat1 */
        $seat1 = factory(Seat::class)->create();
        Storage::fake('public');
        $image = UploadedFile::fake()->image('photo1.jpg');
        /** @var SeatBookServiceContract $service */
        $service = app(SeatBookService::class);
        $service->setSeat($seat1->id)
                ->setImage($image)
                ->bookFor([
                    'name'    => 'Bob',
                    'company' => 'Company1',
                    'email'   => 'email@email.com',
                    'phone'   => '123456',
                ]);

        // act
        $response = $this->json('get', route('events.seats', ['event' => $seat1->event_id]));

        // assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'row_number',
                    'seat_number',
                    'price',
                    'is_booked',
                    'visitor_logo',
                ],
            ],
        ]);
        $result = $response->decodeResponseJson();
        $this->assertCount(1, $result['data']);
        $seatTest = $result['data'][0];
        $this->assertTrue($seatTest['is_booked']);
        $this->assertEquals($seat1->visitor->image->getUrl(), $seatTest['visitor_logo']);
    }

    /** @test */
    public function testShouldUseService()
    {
        // setup | pre assert
        /** @var Event $event */
        $event = factory(Event::class)->create();
        $this->mock(SeatListServiceContract::class, function (MockInterface $mock) use ($event) {
            $mock->shouldReceive('allForEventId')
                 ->with($event->id)
                 ->once()
                 ->andReturn(collect());
        });

        // act
        $this->json('get', route('events.seats', ['event' => $event->id]));
    }
}
