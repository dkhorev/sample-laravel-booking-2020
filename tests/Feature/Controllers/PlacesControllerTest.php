<?php

namespace Tests\Feature\Controllers;

use App\Contracts\Services\PlaceListServiceContract;
use App\Models\Event;
use App\Models\Place;
use Illuminate\Http\Response;
use Mockery\MockInterface;
use Tests\TestCase;

class PlacesControllerTest extends TestCase
{
    /** @test */
    public function testIndex()
    {
        // setup
        factory(Place::class, 5)->create();
        Place::all()->each(function (Place $place) {
            factory(Event::class)->create(['place_id' => $place->id]);
        });

        // act
        $response = $this->json('get', route('places.index'));

        // assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'address',
                    'phone',
                    'location' => [
                        'lon',
                        'lat',
                    ],
                    'events'   => [
                        [
                            'id',
                            'name',
                            'starts_at',
                            'seat_count',
                            'visitor_count',
                        ],
                    ],
                ],
            ],
        ]);
        $result = $response->decodeResponseJson();
        $this->assertCount(5, $result['data']);
    }

    /** @test */
    public function testShouldUseService()
    {
        // setup | pre assert
        $this->mock(PlaceListServiceContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('all')
                 ->once()
                 ->andReturn(collect());
        });

        // act
        $this->json('get', route('places.index'));
    }
}
